import 'package:flutter/material.dart';

import 'home_screen.dart';
import 'json_parsing_map.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.purple,
      ),
      debugShowCheckedModeBanner: false,
      home: JsonParsingMap(),
    );
  }
}
