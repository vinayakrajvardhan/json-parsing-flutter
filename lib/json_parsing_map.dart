import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart';

import 'models/posts.dart';

class JsonParsingMap extends StatefulWidget {
  @override
  _JsonParsingMapState createState() => _JsonParsingMapState();
}

class _JsonParsingMapState extends State<JsonParsingMap> {
  Future<PostList> data;
  final String url = "https://jsonplaceholder.typicode.com/posts";

  @override
  void initState() {
    super.initState();
    Network network = Network(url: url);
    data = network.loadPosts();
    print(data);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("PODO"),
        centerTitle: true,
      ),
      body: Center(
        child: Container(
          child: FutureBuilder(
            future: data,
            builder: (context, AsyncSnapshot<PostList> snapshot) {
              List<Posts> allPosts;
              if (snapshot.hasData) {
                allPosts = snapshot.data.posts;
                return CreateListView<PostList>(context, snapshot.data.posts);
              } else {
                return CircularProgressIndicator();
              }
            },
          ),
        ),
      ),
    );
  }

  Widget CreateListView<PostList>(context, List<Posts> data) {
    return ListView.separated(
      separatorBuilder: (context, index) => Divider(
        color: Colors.black12,
      ),
      itemCount: data.length,
      itemBuilder: (context, index) {
        return Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ListTile(
              leading: CircleAvatar(
                child: Text("${data[index].id}"),
                backgroundColor: Colors.teal,
              ),
              title: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text("${data[index].title}"),
              ),
              subtitle: Text("${data[index].body}"),
            ),
          ],
        );
      },
    );
  }
}

class Network {
  final String url;
  Network({this.url});

  Future<PostList> loadPosts() async {
    final response = await get(Uri.encodeFull(url));
    if (response.statusCode == 200) {
      return PostList.fromMap(json.decode(response.body));
    } else {
      throw Exception("failed to get data");
    }
  }
}
