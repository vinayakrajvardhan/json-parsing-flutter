import 'dart:convert';

import 'package:flutter/foundation.dart';

// class PostList {
//   final List<Posts> posts;
//   PostList({this.posts});

//   factory PostList.fromJson(List<dynamic> parsedjson) {
//     List<Posts> posts = [];
//     posts = parsedjson.map((i) => Posts.fromjson(i)).toList();

//     return PostList(posts: posts);
//   }
// }

// class Posts {
//   final int id;
//   final int userid;
//   final String title;
//   final String body;

//   Posts({this.body, this.id, this.title, this.userid});

//   factory Posts.fromjson(Map<String, dynamic> json) {
//     return Posts(
//       id: json['id'],
//       title: json['title'],
//       body: json['body'],
//       userid: json['userid'],
//     );
//   }
// }

class PostList {
  final List<Posts> posts;

  PostList({
    this.posts,
  });

  factory PostList.fromMap(List<dynamic> parsedjson) {
    List<Posts> posts = [];
    posts = parsedjson.map((i) => Posts.fromMap(i)).toList();
    return PostList(posts: posts);
  }
}

class Posts {
  final int id;
  final int userId;
  final String body;
  final String title;

  Posts({this.id, this.userId, this.body, this.title});

  factory Posts.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return Posts(
      id: map['id'],
      userId: map['userId'],
      body: map['body'],
      title: map['title'],
    );
  }
}
